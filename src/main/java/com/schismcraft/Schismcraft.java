package com.schismcraft;

import com.schismcraft.database.Database;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.server.ServerStartingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod("schismcraft")
public class Schismcraft
{
    public static final String MOD_NAME = "Schismcraft";
    public static final String MOD_ID = "schismcraft";

    public Schismcraft()
    {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        MinecraftForge.EVENT_BUS.register(this);

        Log.info("core", "Loading!");
        Database.get().loadJson();
    }

    /**
     * Called during mod setup, performs mod initialisation.
     * @param event The forge setup event.
     */
    private void setup(final FMLCommonSetupEvent event)
    {
        Log.info("core", "Starting up!");
    }

    /**
     * Called during server startup, performs server side only mod initialisation.
     * @param event The forge server starting event.
     */
    @SubscribeEvent
    public void onServerStarting(ServerStartingEvent event) {
        Log.info("core", "Server side startup!");
    }

    /**
     * Send IMC Events, currently unused.
     * @param event The IMC event.
     */
    private void enqueueIMC(final InterModEnqueueEvent event)
    {
        //
    }

    /**
     * Process IMC Events, currently unused.
     * @param event The IMC event.
     */
    private void processIMC(final InterModProcessEvent event)
    {
        //
    }
}
