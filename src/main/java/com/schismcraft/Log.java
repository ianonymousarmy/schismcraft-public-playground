package com.schismcraft;

import com.schismcraft.database.Database;
import com.schismcraft.database.IDatabaseEventListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;

/**
 * This class is used for quickly logging things, this covers simple text logging but can also handle other common debug output, etc.
 */
public class Log implements IDatabaseEventListener
{
	/**
	 * The singleton log instance.
	 */
	protected static final Log INSTANCE = new Log();

	/**
	 * The logger to use.
	 */
	protected final Logger logger = LogManager.getLogger();

	protected Log()
	{
		Database.get().addLoadListener(this);
	}

	@Override
	public void onDatabaseLoaded(Database database)
	{
		info("log", "Received schismcraft database load event!");
	}

	/**
	 * Logs a message at info level.
	 * @param category The category of the message, should be lowercase and subject to config control, ex: core, ai, equipment, dungeon.
	 * @param message The message to display.
	 * @param objects Optional additional objects to include in the message.
	 */
	public static void info(String category, String message, Object ...objects)
	{
		// TODO Check if category info logging is enabled in the config.
		String prefix = "[" + Schismcraft.MOD_NAME + "|" + category.toLowerCase(Locale.ROOT) + "] ";
		if (objects.length > 0) {
			INSTANCE.logger.info(prefix + message, objects);
		} else {
			INSTANCE.logger.info(prefix + message);
		}
	}

	/**
	 * Logs a message at info level and in the debug category.
	 * @param message The message to display.
	 * @param objects Optional additional objects to include in the message.
	 */
	public static void info(String message, Object ...objects)
	{
		info("debug", message, objects);
	}

	/**
	 * Logs a message at error level.
	 * @param category The category of the message, should be lowercase and subject to config control, ex: core, ai, equipment, dungeon.
	 * @param message The message to display.
	 * @param objects Optional additional objects to include in the message.
	 */
	public static void error(String category, String message, Object ...objects)
	{
		// TODO Check if category info logging is enabled in the config.
		String prefix = "[" + Schismcraft.MOD_NAME + "]" + "[" + category.toLowerCase(Locale.ROOT) + "] ";
		if (objects.length > 0) {
			INSTANCE.logger.error(prefix + message, objects);
		} else {
			INSTANCE.logger.error(prefix + message);
		}
	}
}
