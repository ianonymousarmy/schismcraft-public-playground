package com.schismcraft.managers;

import com.schismcraft.Log;
import com.schismcraft.database.Database;
import com.schismcraft.database.IDatabaseEventListener;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
public class BlockManager implements IDatabaseEventListener
{
	protected static final BlockManager INSTANCE = new BlockManager();

	/**
	 * Gets the singleton BlockManager instance.
	 * @return The Block Manager, this manages everything related to blocks from configs, to registration and loading.
	 */
	public static BlockManager get()
	{
		return INSTANCE;
	}

	@SubscribeEvent
	public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent)
	{
		Log.info("blocks", "Received forge register block event!");
	}

	@Override
	public void onDatabaseLoaded(Database database)
	{
		Log.info("blocks", "Received schismcraft database load event!");
	}
}
