package com.schismcraft.database;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.schismcraft.Log;
import com.schismcraft.Schismcraft;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the core mod database that stores all information loaded from every json config and potentially other sources.
 */
public class Database
{
	protected static final Database INSTANCE = new Database();

	/**
	 * Gets the singleton Database instance.
	 * @return The core mod Database, this holds all information loaded from configs.
	 */
	public static Database get()
	{
		return INSTANCE;
	}

	/**
	 * A list of database event listeners, these are called whenever data is loaded or reloaded.
	 */
	private final List<IDatabaseEventListener> loadListeners = new ArrayList<>();

	/**
	 * A list of data sources (currently on json sources), these are used to locate data to load in.
	 */
	private final List<DataSource> jsonDataSources = new ArrayList<>();

	/**
	 * A map of the latest raw json data that has been loaded into this database by data source.
	 */
	private final Map<DataSource, List<DatabaseJson>> jsonDatabase = new HashMap<>();

	public Database()
	{
		this.jsonDataSources.add(new DataSource("common", DataSource.Location.INTERNAL));
		this.jsonDataSources.add(new DataSource("override", DataSource.Location.CONFIG));
	}

	/**
	 * Adds a new database data load event listener, these are called (in the order that they were added) whenever data is loaded or reloaded.
	 * @param loadListener The load event listener to add.
	 */
	public void addLoadListener(IDatabaseEventListener loadListener)
	{
		this.loadListeners.add(loadListener);
	}

	/**
	 * Calls all load event listeners (in the order that they were added starting from the first added), this should only be called when data is loaded or reloaded so that listeners can react to the data changes.
	 */
	protected void onLoaded()
	{
		this.loadListeners.forEach(loadListener -> {
			loadListener.onDatabaseLoaded(this);
		});
	}

	/**
	 * Loads (or reloads) all json configs into the database, first from common (internal json), then from config defaults and then config overrides.
	 */
	public void loadJson()
	{
		this.jsonDatabase.clear();
		this.jsonDataSources.forEach(this::loadJsonFromSource);
		this.generateReferenceConfig();
		this.onLoaded();
	}

	/**
	 * Loads json from the provided data source.
	 * @param dataSource The data source to load the json from.
	 */
	public void loadJsonFromSource(DataSource dataSource)
	{
		Log.info("database", "Loading json data from " + dataSource);
		List<DatabaseJson> databaseJsons = dataSource.loadJson(this.jsonDatabase);
		this.jsonDatabase.put(dataSource, databaseJsons);
		databaseJsons.forEach(databaseJson -> Log.info("database", "Loaded JSON: " + databaseJson));
	}

	/**
	 * Generates the reference config json files, these are for users to copy into the override configs.
	 */
	public void generateReferenceConfig()
	{
		final List<DatabaseJson> defaultDatabaseJsons = new ArrayList<>();
		this.jsonDatabase.forEach((dataSource, databaseJsons) -> {
			if (dataSource.location() == DataSource.Location.CONFIG) {
				return;
			}
			defaultDatabaseJsons.addAll(databaseJsons);
		});

		String configReferencePath = (new File(".")) + "/config/" + Schismcraft.MOD_ID + "/reference/";
		try {
			FileUtils.deleteDirectory(new File(configReferencePath));
		} catch (IOException e) {
			Log.error("datasource", "Error clearing config reference folder: " + configReferencePath);
			throw new RuntimeException(e);
		}

		Gson gson = (new GsonBuilder()).setPrettyPrinting().disableHtmlEscaping().create();
		defaultDatabaseJsons.forEach(defaultDatabaseJson -> {
			File jsonFile = new File(configReferencePath + defaultDatabaseJson.relativePath());
			jsonFile.getParentFile().mkdirs();
			try {
				jsonFile.createNewFile();
				FileOutputStream outputStream = new FileOutputStream(jsonFile);
				OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
				outputStreamWriter.append(gson.toJson(defaultDatabaseJson.jsonObject()));
				outputStreamWriter.close();
				outputStream.close();
			} catch (IOException e) {
				Log.error("datasource", "Error created reference config json file or folder: " + jsonFile);
				throw new RuntimeException(e);
			}
		});
	}
}
