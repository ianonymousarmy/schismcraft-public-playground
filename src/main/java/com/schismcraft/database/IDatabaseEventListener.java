package com.schismcraft.database;

public interface IDatabaseEventListener
{
	/**
	 * Called when the database has data loaded or reloaded.
	 */
	public void onDatabaseLoaded(Database database);
}
